﻿using Hangman.ViewModels;
using System;
using System.Collections.Generic;

using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Hangman
{
    /// <summary>
    /// Interaction logic for GameView.xaml
    /// </summary>
    public partial class GameView : UserControl
    {
        public GameView()
        {
            DataContext = new GameVM();
            InitializeComponent();

        }

        private void letter_Click(object sender, RoutedEventArgs e)
        {
            Button button = sender as Button;
            GameVM game = DataContext as GameVM;
            game.Letter = button.Content.ToString()[0];
            button.IsEnabled = false;
            
        }

        private void reset()
        {
            btn_a.IsEnabled =
            btn_b.IsEnabled =
            btn_c.IsEnabled =
            btn_d.IsEnabled =
            btn_e.IsEnabled =
            btn_f.IsEnabled =
            btn_g.IsEnabled =
            btn_h.IsEnabled =
            btn_i.IsEnabled =
            btn_j.IsEnabled =
            btn_k.IsEnabled =
            btn_l.IsEnabled =
            btn_m.IsEnabled =
            btn_n.IsEnabled =
            btn_o.IsEnabled =
            btn_p.IsEnabled =
            btn_q.IsEnabled =
            btn_r.IsEnabled =
            btn_s.IsEnabled =
            btn_t.IsEnabled =
            btn_u.IsEnabled =
            btn_v.IsEnabled =
            btn_w.IsEnabled =
            btn_x.IsEnabled =
            btn_y.IsEnabled =
            btn_z.IsEnabled =
            true;
        }
        private void greseli(int nr = 0)
            {
                img_Status.Source = new BitmapImage(new Uri("pack://application:,,,/Images/Hangman" + nr.ToString() + ".png"));
            }

        private void New_Game(object sender, RoutedEventArgs e)
        {
            reset();
        }

        private void All_type_Select(object sender, RoutedEventArgs e)
        {
            MI_All_Types.IsChecked =
            MI_Bug.IsChecked =
            MI_Dark.IsChecked =
            MI_Dragon.IsChecked =
            MI_Electrick.IsChecked =
            MI_Fairy.IsChecked =
            MI_Fighting.IsChecked =
            MI_Fire.IsChecked =
            MI_Flyng.IsChecked =
            MI_Ghost.IsChecked =
            MI_Grass.IsChecked =
            MI_Ground.IsChecked =
            MI_Ice.IsChecked =
            MI_Normal.IsChecked =
            MI_Poison.IsChecked =
            MI_Psychic.IsChecked =
            MI_Rock.IsChecked =
            MI_Steel.IsChecked =
            MI_Water.IsChecked = true;
        
        }
        private void Category_Selected(object sender, RoutedEventArgs e)
        {
            (sender as MenuItem).IsChecked = !(sender as MenuItem).IsChecked;
        }
    }
    } 
