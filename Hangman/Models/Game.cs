﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hangman
{
    class Game : INotifyPropertyChanged
    {

        private string _word="TEST";
        private List<Char> letters;
        private char Letter
        {
            set
            {
                if (letters == null)
                    letters = new List<char>();
                if (!letters.Contains(value))
                    letters.Add(value);
                OnPropertyChanged("World");
            }

        }
        public string World
        {
           
            get
            {
                string toreturn=string.Empty;
                foreach (char chars in _word)
                {
                    if (letters.Contains(chars))
                        toreturn += chars;
                    else
                        toreturn += "_";
                }
                return toreturn;
            }
        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion
    }
}
