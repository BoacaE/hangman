﻿using ICommandServices.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Media.Imaging;

namespace Hangman.ViewModels
{
    class GameVM :BaseVM
    {

        private string _word = "Test";
        public string Word
        {
            get { return _word; }
            set { _word = value; }
        }
        private List<Char> letters;
        public char Letter
        {
            set
            {
                if (letters == null)
                    letters = new List<char>();
                if (!letters.Contains(value))
                    letters.Add(value);
              
                OnPropertyChanged("ToFind");
                OnPropertyChanged("ResetButton");
                ResetButton = true;
            }

        }
        public string ToFind
        {
            
            get
            {
                string toreturn = string.Empty;
                foreach (char chars in _word)
                {
                    
                    if (letters != null)
                    {
                        if (letters.Contains(char.ToUpper(chars)))
                        {
                            toreturn += chars;
                            continue;
                        }
                    }
                    
                        toreturn += "-";
                }
                return toreturn;
            }
        }
        public void newGame()
        {

        }
        private bool _reset = true;
        public Boolean ResetButton
        {
            set
            {
                _reset = value;
                OnPropertyChanged("ResetButton");
            }
            get { return false; }
        }
        private BitmapImage _statusImage;
        public BitmapImage StatusImage
        {
            set
            {
                _statusImage = value;
                OnPropertyChanged("StatusImage");
            }
            get
            {
                return _statusImage;
            }
        }

        private BitmapImage _avatarImage;
        public BitmapImage avatarImage
        {
            set
            {
                _avatarImage = value;
                OnPropertyChanged("StatusImage");
            }
            get
            {
                return _avatarImage;
            }
        }
        private BitmapImage _hintImage;
        public BitmapImage hintImage
        {
            set
            {
                _hintImage = value;
                OnPropertyChanged("StatusImage");
            }
            get
            {
                return _hintImage;
            }
        }
    }
}
